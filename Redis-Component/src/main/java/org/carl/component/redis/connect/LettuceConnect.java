/*
 * Copyright 2021 carllongj
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

package org.carl.component.redis.connect;

import io.lettuce.core.RedisClient;
import io.lettuce.core.RedisFuture;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.async.RedisAsyncCommands;

import java.util.concurrent.ExecutionException;

/**
 * @author carllongj
 * 2021/4/6 21:05
 */
public class LettuceConnect {

	public static final String host = "192.168.222.130";

	public static final int port = 6379;

	public static void main(String[] args) throws ExecutionException, InterruptedException {
		RedisClient client = RedisClient.create(String.format("redis://%s:%d", host, port));
		StatefulRedisConnection<String, String> connection = client.connect();

		RedisAsyncCommands<String, String> commands = connection.async();
		RedisFuture<String> redisFuture = commands.get("lettuce");
		System.out.println(redisFuture.get());
	}
}
